const fs = require('fs');
const path = require('path');


const directoryCreate =async (nameDirectory) => {
    const directoryPath = path.join(__dirname, nameDirectory);
    fs.mkdir(directoryPath, { recursive: true }, (error) => {
        if (error) {
            throw new Error(error);
        }
    });
    return directoryPath;
};

const createRandomJsonFile = async (directoryPath, num) => {

    for (let file = 1; file <= num; file++) {
        let fileName = Math.random().toString().slice(2) + '.json';
        let pathFile = path.join(directoryPath, fileName);
        fs.open(pathFile, 'w', (error) => {
            if (error) {
                throw new Error(error);
            }
        })
    }

    return directoryPath;
};

const deleteFileSimultaneously = async (directoryPath,callbackFunction) => {
    fs.readdir(directoryPath, (error, data) => {
        if (error) {
            throw new Error(error);
        }
        else {
            data.map((fileName) => {
                const filePath = path.join(directoryPath, fileName);
                fs.unlink(filePath, (error) => {
                    if (error) {
                        throw new Error(error);
                    }
                    else{
                        callbackFunction("Files deleted Successfully")
                    }
                })
            });
        }
    })
    
};



module.exports.directoryCreate = directoryCreate;
module.exports.createRandomJsonFile = createRandomJsonFile;
module.exports.deleteFileSimultaneously = deleteFileSimultaneously;
