const fs = require('fs');
const path = require('path');


const readFiles = () => {
    return new Promise((resolve, reject) => {
        let pathFile = path.join(__dirname, 'lipsum.txt');
        fs.readFile(pathFile, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
};

const convertContentUpperCase = (data) => {
    return new Promise((resolve, reject) => {
        let dataUpper = data.toUpperCase();
        let upperPathFile = path.join(__dirname, 'upperLipsum.txt');
        let filesNamePath = path.join(__dirname, 'filesNames.txt');
        fs.writeFile(upperPathFile, dataUpper, (error) => {
            if (error) {
                reject(error);
            }
            else {
                fs.writeFile(filesNamePath, upperPathFile, (error) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(data);
                    }
                })
            }
        })
    })
};

const convertContentLowerCase = () => {
    return new Promise((resolve, reject) => {
        let filePath = path.join(__dirname, 'upperLipsum.txt');
        let lowerCaseFile = path.join(__dirname, 'lowerLipsum.txt');
        fs.readFile(filePath, 'utf8', (error, data) => {
            if (error) {
                reject(error);
            }
            else {
                let lowerCaseData = data.toLowerCase();
                let dataInSentences = lowerCaseData.split('.').join('\n');
                fs.writeFile(lowerCaseFile, dataInSentences, (error) => {
                    if (error) {
                        reject(error)
                    }
                    else {
                        let lowercaseFilePath = path.join(__dirname, 'lowerLipsum.txt');
                        let filesNamesPath = path.join(__dirname, 'filesNames.txt');
                        fs.writeFile(filesNamesPath, '\n' + lowercaseFilePath, { flag: 'a' }, (error) => {
                            if (error) {
                                reject(error)
                            }
                            else {
                                resolve();
                            }
                        })
                    }
                })

            }

        });
    });

}

const sortContentOfNewFiles = () => {
    return new Promise((resolve, reject) => {
        const upperCaseDataPath = path.join(__dirname, 'upperLipsum.txt');
        const lowerCaseDataPath = path.join(__dirname, 'lowerLipsum.txt');
        fs.readFile(upperCaseDataPath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                let upperCaseData = data;
                fs.readFile(lowerCaseDataPath, 'utf-8', (err, data) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        let lowerCaseData = data;
                        let newData = upperCaseData + lowerCaseData;
                        let sortedData = newData.split(' ').sort().join('\n');
                        let sortedDataPathFile = path.join(__dirname, 'sortedDataFile.txt');
                        fs.writeFile(sortedDataPathFile, sortedData, (error) => {
                            if (error) {
                                reject(error);
                            }
                            else {
                                let filesNamePath = path.join(__dirname, 'filesNames.txt')
                                fs.writeFile(filesNamePath, '\n' + sortedDataPathFile, { flag: 'a' }, (error) => {
                                    if (error) {
                                        reject(error)
                                    }
                                    else {
                                        resolve();
                                    }
                                })
                            }
                        })

                    }

                })
            }
        })
    })
}

const readDeleteFiles = () => {
    return new Promise((resolve, reject) => {

        let filesNamePath = path.join(__dirname, 'filesNames.txt');
        fs.readFile(filesNamePath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                let dataList = data.split('\n');
                dataList.forEach((item) => {
                    fs.unlink(item, (error) => {
                        if (error) {
                            reject(error);
                        }
                    })
                })
                resolve("Files deleted");

            }
        })
    })

}


module.exports = { readFiles, convertContentUpperCase, convertContentLowerCase, sortContentOfNewFiles, readDeleteFiles };















