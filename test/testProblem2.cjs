let {convertContentUpperCase , readFiles, convertContentLowerCase, sortContentOfNewFiles,readDeleteFiles}=require("../problem2.cjs")

async function ayncAwait(){
    try{
    let data = await readFiles();
    let data1 = await convertContentUpperCase(data);
    await convertContentLowerCase(data1);
    await sortContentOfNewFiles();
    let result=await readDeleteFiles();
    console.log(result);
    }
    catch(error){
        console.log(error);
    }

}
ayncAwait();



