const {directoryCreate,createRandomJsonFile,deleteFileSimultaneously} = require("../problem1.cjs");


async function ayncAwait(){
     try{
      let result= await directoryCreate('JsonDirectory')
      let resultFile = await createRandomJsonFile(result,5);
      await deleteFileSimultaneously(resultFile,(data)=>{
         console.log(data);
      });
     }
     catch(error){
        console.log(error);
     }

}
ayncAwait();